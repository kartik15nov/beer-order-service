package com.ub.beerOrderService.domain;

public enum OrderStatusEnum {
    NEW, READY, PICKED_UP
}
